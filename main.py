#!/usr/bin/python3
from debug.debug import *
info("Loaded debug library")

from mainloop import *
info("Loaded main loop")

def main():
	info("Entering main loop")
	setup()
	while(1):
		mainloop()

main()
