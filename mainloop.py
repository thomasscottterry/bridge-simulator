from debug.debug import *
info("Loaded debug library")
import sys
info("Loaded system library")
import pygame
info("Loaded pygame")
from pygame.locals import *
from gamelib.colors import *
from terminals import * 
from gamelib.font import *
import application
console = []
FPS = 60
fpsClock = pygame.time.Clock() 

def setup():
	global console
	global DISPLAYSURF
	pygame.init()
	DISPLAYSURF = pygame.display.set_mode((1600,1200),pygame.FULLSCREEN, 32)
	pygame.display.set_caption('Bridge Simulator')
	if(len(sys.argv) > 1):
		console = get_cons(sys.argv[1])
	else:
		console = get_cons("none")
	DISPLAYSURF.fill(BLACK)
	drawheader("Bridge Simulator", DISPLAYSURF, (400,500), GREEN)
	pygame.display.update()
	pygame.mixer.music.load('music/theme.mp3')
	pygame.mixer.music.play()
	application.waitkey()
	

def mainloop():	
	global DISPLAYSURF
	global console
	DISPLAYSURF.fill(BLACK)
	fpsClock.tick(FPS)
	application.doEvents()
	pygame.display.update()
	
	
def get_cons(console):
	if(console == "comms"):
		return comms
	elif(console == "nav"):
		return nav
	elif(console == "main"):
		return mainscreen
	else:
		return mainscreen
		
