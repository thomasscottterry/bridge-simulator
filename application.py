import pygame,sys
from pygame.locals import *
from debug.debug import *

def doEvents():
	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			endgame()
		elif event.type == pygame.KEYDOWN and event.key == K_ESCAPE:
			pygame.quit()
			endgame()
	
def waitkey():
	while(1):
		for event in pygame.event.get():
			if event.type == pygame.KEYDOWN:
				return
