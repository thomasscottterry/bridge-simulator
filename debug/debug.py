from inspect import getframeinfo, stack
from sys import exit
def info(string):
	caller = getframeinfo(stack()[1][0])
	print("[INFO](" + caller.filename + ":" + caller.lineno.__str__() + "): " + string)

def warn(string):
	caller = getframeinfo(stack()[1][0])
	print("[WARNING](" + caller.filename + ":" + caller.lineno.__str__() + "): " + string)

def err(string):
	caller = getframeinfo(stack()[1][0])
	print("[ERROR](" + caller.filename + ":" + caller.lineno.__str__() + "): " + string)
	# cleanup()
	exit(1)

def endgame():
        caller = getframeinfo(stack()[1][0])
        print("[EVENT](" + caller.filename + ":" + caller.lineno.__str__() + "): Exiting Game")
        # cleanup()
        exit(1)
