all:
	@pyinstaller main.py --onefile > build.log 2>&1
	@cd mainscreen && make
	@cp dist/main bridge >> build.log 2>&1
	@rm -r dist main.spec build >> build.log 2>&1

clean:
	rm bridge build.log -f
	rm `find . -name __pycache__` -rf
	@cd mainscreen && make clean

