from pygame import font
from gamelib import colors
font.init()
mainfont = font.Font(font.get_default_font(),20)
headerfont = font.Font(font.get_default_font(), 100)
def drawtext(text, surface, point):
	coloredtext(text, surface, point, colors.BLUE)
	
	
def coloredtext(text, surface, point, color):
	actext = mainfont.render(text, 1, color)
	surface.blit(actext, point)
	
def drawheader(text, surface, point, color):
	actext = headerfont.render(text, 1, color)
	surface.blit(actext, point)
